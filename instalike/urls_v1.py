from django.conf.urls import url, include
from rest_framework.urlpatterns import format_suffix_patterns

urlpatterns = \
    [
        url(r'^authorization/', include('apps.authorization.urls.authorization', namespace='authorization')),
        url(r'^posts/', include('apps.posts.urls.post', namespace='posts')),
        url(r'^followers/', include('apps.followers.urls.followers', namespace='followers')),
    ]

urlpatterns = format_suffix_patterns(urlpatterns)
