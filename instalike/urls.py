from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from rest_framework.documentation import include_docs_urls

from apps.common.index import index

urlpatterns = \
    [
        url(r'^$', index),
        url(r'^admin/', admin.site.urls),
        url(r'^api/v1/', include('instalike.urls_v1', namespace='api_v1')),
        url(r'^docs/', include_docs_urls(title='InstaLike')),
    ]

if settings.DEBUG:
    urlpatterns.append(url(r'^rest-framework/', include('rest_framework.urls', namespace='rest_framework')))
    urlpatterns.extend(static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT))
    urlpatterns.extend(static(settings.STATIC_URL, document_root=settings.STATIC_ROOT))
