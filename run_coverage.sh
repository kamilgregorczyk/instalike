#!/usr/bin/env bash
coverage run --source='.' manage.py test --no-input
coverage report --fail-under=70 --skip-covered
