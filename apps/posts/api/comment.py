from rest_framework import generics
from rest_framework.permissions import IsAuthenticatedOrReadOnly

from apps.common.permissions import IsAuthorOrReadOnly
from apps.posts.models.comment import Comment
from apps.posts.serializers.comment import CommentSerializer


class CommentListView(generics.ListCreateAPIView):
    """
    View that's responsible for creating new comments and retrieving a list of them.
    """
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.select_related("author", "post").filter(post_id=self.kwargs['post_id'])

    def perform_create(self, serializer):
        serializer.save(author=self.request.user, post_id=self.kwargs['post_id'])


class CommentDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    View that's responsible for updating (partially and fully), retrieving and deleting comments.
    """
    permission_classes = (IsAuthorOrReadOnly,)
    serializer_class = CommentSerializer

    def get_queryset(self):
        return Comment.objects.select_related("author", "post").filter(post_id=self.kwargs['post_id'])
