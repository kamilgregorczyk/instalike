from django.db.models import Count, Subquery, Q
from rest_framework import generics
from rest_framework.filters import SearchFilter, OrderingFilter
from rest_framework.permissions import IsAuthenticatedOrReadOnly, IsAuthenticated

from apps.common.permissions import IsAuthorOrReadOnly
from apps.followers.models.followers import Followers
from apps.posts.models.post import Post
from apps.posts.serializers.post import PostSerializer, PostDetailSerializer


class PostListView(generics.ListCreateAPIView):
    """
    View that's responsible for creating new posts and retrieving a list of them.
    """
    queryset = Post.objects.select_related('author').prefetch_related('comments').all().annotate(
        comments_count=Count('comments__id'))
    permission_classes = (IsAuthenticatedOrReadOnly,)
    serializer_class = PostSerializer
    search_fields = ["title", "description", "author__username"]
    ordering_fields = ["title", "created_at", "updated_at"]
    filter_backends = (OrderingFilter, SearchFilter,)

    def perform_create(self, serializer):
        serializer.save(author=self.request.user)

    def get(self, request, *args, **kwargs):
        return super(PostListView, self).get(request, args, kwargs)


class PostDetailView(generics.RetrieveUpdateDestroyAPIView):
    """
    View that's responsible for updating (partially and fully), retrieving and deleting posts.
    """
    queryset = Post.objects.all().annotate(comments_count=Count('comments__id'))
    permission_classes = (IsAuthorOrReadOnly,)
    serializer_class = PostDetailSerializer


class StreamView(generics.ListAPIView):
    """
    Returns a stream of photos based on followers.
    """
    permission_classes = (IsAuthenticated,)
    serializer_class = PostSerializer

    def get_queryset(self):
        return Post.objects.filter(
            Q(author__in=Subquery(Followers.objects.filter(user_id=self.request.user.id).values('follower_id')))
            | Q(author=self.request.user))
