from rest_framework import serializers
from rest_framework.reverse import reverse

from apps.authorization.serializers.user import UserSerializer
from apps.posts.models.comment import Comment


class CommentSerializer(serializers.ModelSerializer):
    author = UserSerializer(read_only=True)
    url = serializers.SerializerMethodField()

    def get_url(self, instance):
        return reverse("api_v1:posts:comments:comment-detail", args=[instance.post.id, instance.id],
                       request=self.context['request'])

    class Meta:
        model = Comment
        read_only_field = ['created_at']
        fields = ['id', 'description', 'url', 'author', 'created_at', 'updated_at']
