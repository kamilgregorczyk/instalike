from rest_framework import serializers

from apps.authorization.serializers.user import UserSerializer
from apps.posts.models.post import Post


class PostSerializer(serializers.ModelSerializer):
    url = serializers.HyperlinkedIdentityField("api_v1:posts:post-detail")
    author = UserSerializer(read_only=True)
    comments_count = serializers.IntegerField(read_only=True)

    class Meta:
        model = Post
        read_only_fields = ['author', 'created_at']
        fields = ['id', 'title', 'description', 'url', 'author', 'image', 'comments_count', 'created_at', 'updated_at']


class PostDetailSerializer(PostSerializer):
    class Meta(PostSerializer.Meta):
        read_only_fields = PostSerializer.Meta.read_only_fields + ['image']
        fields = PostSerializer.Meta.fields
