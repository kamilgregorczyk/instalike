from django.contrib.auth.models import User
from django.db import models

from apps.common.pagination import StandardResultsSetPagination


def user_directory_path(instance, filename):
    # All files will be uploaded to MEDIA_ROOT/posts/user_ID/filename
    return 'posts/user_{0}/{1}'.format(instance.author.id, filename)


class Post(models.Model):
    author = models.ForeignKey(User)

    title = models.CharField(max_length=255)
    description = models.TextField(max_length=500)
    image = models.ImageField(upload_to=user_directory_path)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        ordering = ["-created_at"]

    def __str__(self):
        return self.title

    def get_first_comments(self):
        return self.comments.all()[:StandardResultsSetPagination.page_size]
