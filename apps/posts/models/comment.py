from django.contrib.auth.models import User
from django.db import models

from apps.posts.models.post import Post


class Comment(models.Model):
    author = models.ForeignKey(User)
    post = models.ForeignKey(Post, related_name="comments")

    description = models.TextField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.description[:100]

    class Meta:
        ordering = ['-created_at']
