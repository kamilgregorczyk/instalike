from django.conf.urls import url, include

from apps.posts.api.post import PostListView, PostDetailView, StreamView

urlpatterns = [

    url(r'^$', PostListView.as_view(), name="post"),
    url(r'^stream/$', StreamView.as_view(), name="stream"),
    url(r'^(?P<pk>[0-9]+)/$', PostDetailView.as_view(), name="post-detail"),
    url(r'(?P<post_id>[0-9]+)/comments/', include('apps.posts.urls.comment', namespace='comments')),
]
