from django.conf.urls import url

from apps.posts.api.comment import CommentListView, CommentDetailView

urlpatterns = [
    url(r'^$', CommentListView.as_view(), name="comment"),
    url(r'^(?P<pk>[0-9]+)$', CommentDetailView.as_view(), name="comment-detail"),
]
