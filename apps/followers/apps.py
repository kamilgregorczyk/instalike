from django.apps import AppConfig


class FollowersConfig(AppConfig):
    name = 'apps.followers'
