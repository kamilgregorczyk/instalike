from django.contrib.auth.models import User
from django.db import models


class Followers(models.Model):
    user = models.ForeignKey(User, related_name='users')
    follower = models.ForeignKey(User, related_name='followers')
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return "{}:{}".format(self.user.email, self.follower.email)

    class Meta:
        ordering = ['-created_at']
        unique_together = ('user', 'follower',)
