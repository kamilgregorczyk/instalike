from rest_framework import serializers

from apps.authorization.serializers.user import UserSerializer
from apps.followers.models.followers import Followers
from apps.posts.models import Post, StandardResultsSetPagination
from apps.posts.serializers.post import PostSerializer


class FollowersSerializer(serializers.ModelSerializer):
    follower = UserSerializer()

    class Meta:
        model = Followers
        read_only_fields = ['follower']
        fields = ['follower']


class SimpleFollowersSerializer(serializers.ModelSerializer):
    def validate_follower(self, follower):
        if self.context['request'].user.id == follower.id:
            raise serializers.ValidationError("Cannot follow yourself")
        return follower

    class Meta:
        model = Followers
        fields = ['follower']


class ProfileSerializer(UserSerializer):
    posts = serializers.SerializerMethodField()
    is_following_count = serializers.SerializerMethodField()
    followed_by_count = serializers.SerializerMethodField()

    def get_followed_by_count(self, user):
        return Followers.objects.filter(follower_id=user.id).count()

    def get_is_following_count(self, user):
        return Followers.objects.filter(user_id=user.id).count()

    def get_posts(self, user):
        return PostSerializer(instance=Post.objects.filter(author_id=user.id)[:StandardResultsSetPagination.page_size],
                              many=True, context={'request': self.context['request']}).data

    class Meta(UserSerializer.Meta):
        fields = UserSerializer.Meta.fields + ["is_following_count", "followed_by_count", "posts"]
