from django.contrib.auth.models import User
from rest_framework import generics
from rest_framework.generics import get_object_or_404
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.views import APIView

from apps.followers.models.followers import Followers
from apps.followers.serializers.followers import FollowersSerializer, ProfileSerializer


class FollowingListView(generics.ListAPIView):
    """
    Returns a list of followers for given user_id.
    """
    serializer_class = FollowersSerializer
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Followers.objects.filter(user_id=self.kwargs['user_id'])


class FollowedByListView(FollowingListView):
    """
    Returns a list of users that are following given user_id
    """

    def get_queryset(self):
        return Followers.objects.filter(follower_id=self.kwargs['user_id'])


class AddFollowerView(APIView):
    """
    By creating a new entry, requested user will start following given follower_id.
    No body required.
    """
    permission_classes = (IsAuthenticated,)

    def post(self, request, *args, **kwargs):
        follower_id = int(kwargs['follower_id'])
        if self.request.user.id == follower_id:
            return Response(status=400, data={"detail": "Cannot follow yourself"})
        if not User.objects.filter(pk=follower_id).exists():
            return Response(status=404, data={"detail": "Follower doesn't exist"})
        obj, created = Followers.objects.get_or_create(user_id=self.request.user.id,
                                                       follower_id=follower_id)
        return Response(status=201 if created else 200)


class RemoveFollowerView(generics.DestroyAPIView):
    """
    By deleting entry, requested user will stop following given follower_id.
    """
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        return Followers.objects.filter(user=self.request.user)

    def get_object(self):
        obj = get_object_or_404(self.get_queryset(), follower_id=self.kwargs['follower_id'])

        # May raise a permission denied
        self.check_object_permissions(self.request, obj)
        return obj


class ProfileView(generics.RetrieveAPIView):
    permission_classes = (IsAuthenticated,)
    serializer_class = ProfileSerializer

    def get_queryset(self):
        return User.objects.prefetch_related("posts").all()
