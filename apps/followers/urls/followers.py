from django.conf.urls import url

from apps.followers.api.followers import AddFollowerView, RemoveFollowerView, ProfileView, \
    FollowedByListView, FollowingListView

urlpatterns = \
    [
        url(r'^profile/(?P<pk>[0-9]+)/$', ProfileView.as_view(), name="profile"),
        url(r'^follow/(?P<follower_id>[0-9]+)/$', AddFollowerView.as_view(), name="followers-add"),
        url(r'^unfollow/(?P<follower_id>[0-9]+)/$', RemoveFollowerView.as_view(), name="followers-remove"),
        url(r'^(?P<user_id>[0-9]+)/followed_by/$', FollowedByListView.as_view(), name="followers-by-list"),
        url(r'^(?P<user_id>[0-9]+)/following/$', FollowingListView.as_view(), name="following-list"),
    ]
