from django.conf.urls import url
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token, refresh_jwt_token

from apps.authorization.api.user import RegisterUserView

urlpatterns = \
    [
        url(r'^register/', RegisterUserView.as_view(), name='create-user'),
        url(r'^get_token/', obtain_jwt_token, name='get-token'),
        url(r'^verify_token/', verify_jwt_token, name='verify-token'),
        url(r'^refresh_token/', refresh_jwt_token, name='refresh-token'),
    ]
