from rest_framework import generics

from apps.authorization.serializers.user import UserSerializer


class RegisterUserView(generics.CreateAPIView):
    """
    View that's responsible fore creating new users.
    """
    serializer_class = UserSerializer
