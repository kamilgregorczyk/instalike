from django.contrib.auth.models import User
from django.contrib.auth.password_validation import validate_password
from rest_framework import serializers


class UserSerializer(serializers.ModelSerializer):
    email = serializers.EmailField(write_only=True)
    username = serializers.CharField(max_length=150)
    password = serializers.CharField(write_only=True)
    password_confirm = serializers.CharField(write_only=True)

    def validate_email(self, email):
        if User.objects.filter(email=email).exists():
            raise serializers.ValidationError("Email is already taken")
        return email

    def validate_username(self, username):
        if User.objects.filter(username=username).exists():
            raise serializers.ValidationError("Username is already taken")
        return username

    def validate_password(self, password):
        tmp_user = User(username=self.initial_data['username'],
                        email=self.initial_data['email'])
        validate_password(password=password, user=tmp_user)
        return password

    def validate_password_confirm(self, password_confirm):
        if password_confirm != self.initial_data['password']:
            raise serializers.ValidationError("Passwords are different!")
        return password_confirm

    def create(self, validated_data):
        validated_data.pop('password_confirm')
        user = User.objects.create(**validated_data)
        user.set_password(validated_data['password'])
        user.save()

        return user

    class Meta:
        model = User
        fields = ['id', 'email', 'username', 'password', 'password_confirm']
