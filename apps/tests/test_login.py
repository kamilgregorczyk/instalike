import json
from time import sleep

from django.contrib.auth.models import User

from apps.common.testcase import InstalikeTestCase
from apps.posts.models import Post


class AuthorizationApiTest(InstalikeTestCase):

    @classmethod
    def setUpTestData(cls):
        super(AuthorizationApiTest, cls).setUpTestData()
        cls.not_valid_jwt_token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9' \
                                  '.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9' \
                                  '.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ'

    def setUp(self):
        self.user_data = {'email': 'ggg@me.com', 'username': 'gggghhhhh', 'password': self.user_password,
                          'password_confirm': self.user_password}

    def test_login_correct_user(self):
        # given
        create_data = {'title': 'Some title', 'description': 'Desc', 'image': self.create_tmp_image()}

        # when
        result = self.login(self.user_1, self.user_password)
        response = json.loads(result.content)

        # create post for test with returned token
        self.authenticate(token=response['token'])
        post_result = self.create_post(**create_data)

        # then
        self.assertEquals(result.status_code, 200)
        self.assertEquals(post_result.status_code, 201)

    def test_login_wrong_password(self):
        # when
        result = self.login(self.user_1, 'ALSDHJLKJADS')
        response = json.loads(result.content)

        # then
        self.assertEquals(result.status_code, 400)
        self.assertEquals(response['non_field_errors'][0], 'Unable to log in with provided credentials.')

    def test_login_not_existing_user(self):
        # when
        result = self.login(User(username='huehehehehhe'), 'hueh')
        response = json.loads(result.content)

        # then
        self.assertEquals(result.status_code, 400)
        self.assertEquals(response['non_field_errors'][0], 'Unable to log in with provided credentials.')

    def test_verify_token_correct_token(self):
        # when
        login_result = self.login(self.user_1, self.user_password)
        login_token = json.loads(login_result.content)['token']

        verified_result = self.verify_token(login_token)
        verified_token = json.loads(verified_result.content)['token']

        # then
        self.assertEquals(verified_result.status_code, 200)
        self.assertEquals(login_result.status_code, 200)
        self.assertEquals(login_token, verified_token)

    def test_verify_token_not_correct_token(self):
        # when
        verified_result = self.verify_token(self.not_valid_jwt_token)
        response = json.loads(verified_result.content)

        # then
        self.assertEquals(verified_result.status_code, 400)
        self.assertEquals(response['non_field_errors'][0], 'Error decoding signature.')

    def test_refresh_token_correct_token(self):
        # given
        create_data = {'title': 'Some title', 'description': 'Desc', 'image': self.create_tmp_image()}

        # when
        get_token_result = self.login(self.user_1, self.user_password)
        first_token = json.loads(get_token_result.content)['token']

        # Let's sleep for 2 seconds so that token refreshes
        sleep(2)

        refresh_token_result = self.refresh_token(first_token)
        refreshed_token = json.loads(refresh_token_result.content)['token']

        # create post for test with returned token
        self.authenticate(token=refreshed_token)
        create_post_result = self.create_post(**create_data)

        # then
        self.assertNotEqual(refreshed_token, first_token)
        self.assertEquals(get_token_result.status_code, 200)
        self.assertEquals(refresh_token_result.status_code, 200)
        self.assertEquals(create_post_result.status_code, 201)

    def test_refresh_token_not_correct_token(self):
        # when
        refresh_token_result = self.refresh_token(self.not_valid_jwt_token)
        response = json.loads(refresh_token_result.content)

        # then
        self.assertEquals(refresh_token_result.status_code, 400)
        self.assertEquals(response['non_field_errors'][0], 'Error decoding signature.')

    def test_register_valid_request(self):
        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 201)
        self.assertIsNotNone(content['id'])
        self.assertEquals(content['username'], self.user_data['username'])

    def test_register_existing_username(self):
        # given
        self.user_data['username'] = self.user_1.username

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'username': ['Username is already taken']})

    def test_register_existing_email(self):
        # given
        self.user_data['email'] = self.user_1.email

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'email': ['Email is already taken']})

    def test_register_password_does_not_match_confirm(self):
        # given
        self.user_data['password_confirm'] = 'blabla'

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'password_confirm': ['Passwords are different!']})

    def test_register_not_valid_email(self):
        # given
        self.user_data['email'] = 'blabla'

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'email': ['Enter a valid email address.']})

    def test_register_not_valid_email_no_domain(self):
        # given
        self.user_data['email'] = 'blabla@goo'

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'email': ['Enter a valid email address.']})

    def test_register_not_valid_email_no_domain(self):
        # given
        self.user_data['email'] = 'blabla@goo'

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'email': ['Enter a valid email address.']})

    def test_register_password_too_short(self):
        # given
        self.user_data['password'] = '#4aa'
        self.user_data['password_confirm'] = self.user_data['password']

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'password': ['This password is too short. It must contain at least 8 characters.']})

    def test_register_password_only_numbers(self):
        # given
        self.user_data['password'] = '12345678'
        self.user_data['password_confirm'] = self.user_data['password']

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'password': ['This password is too common.', 'This password is entirely numeric.']})

    def test_register_password_too_common(self):
        # given
        self.user_data['password'] = 'destiny'
        self.user_data['password_confirm'] = self.user_data['password']

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertTrue('This password is too common.' in content['password'])

    def test_register_password_too_similar_to_username(self):
        # given
        self.user_data['password'] = self.user_data['username']
        self.user_data['password_confirm'] = self.user_data['password']

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'password': ['The password is too similar to the username.']})

    def test_register_password_too_similar_to_email(self):
        # given
        self.user_data['password'] = self.user_data['email']
        self.user_data['password_confirm'] = self.user_data['password']

        # when
        response = self.register_user(self.user_data)
        content = json.loads(response.content)

        # then
        self.assertEquals(response.status_code, 400)
        self.assertEquals(content, {'password': ['The password is too similar to the email address.']})

    @classmethod
    def tearDownClass(cls):
        super(AuthorizationApiTest, cls).tearDownClass()
        Post.objects.all().delete()
        User.objects.all().delete()
