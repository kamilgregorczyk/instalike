import json
from typing import List

from django.urls import reverse

from apps.common.testcase import InstalikeTestCase
from apps.posts.models import User, Post


class PostApiTest(InstalikeTestCase):

    @classmethod
    def setUpTestData(cls):
        super(PostApiTest, cls).setUpTestData()
        cls.created_posts: List[Post] = []
        for i in range(30):
            cls.created_posts.append(
                Post.objects.create(title="Some title", description="Desc", image="bla.png", author=cls.user_1))

    def setUp(self):
        super(PostApiTest, self).setUp()
        self.create_data = {"title": "Some title", "description": "Desc", "image": self.create_tmp_image()}
        self.update_data = {"title": "blabla", "description": "hehe", "image": self.create_tmp_image()}

    def test_get_list(self):
        # when
        result = self.get_posts()
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertIsNotNone(response['next'])
        self.assertIsNone(response['previous'])
        self.assertEqual(response['count'], 30)

        for index, response_post in enumerate(response['results']):
            created_post = self.created_posts[index]
            self.assertEqual(response_post['title'], created_post.title)
            self.assertEqual(response_post['description'], created_post.description)
            self.assertIn(
                reverse("api_v1:posts:post-detail", kwargs={"pk": response_post['id']}), response_post['url'])
            self.assertEqual(response_post['author'], {"id": self.user_1.id, "username": self.user_1.username})
            self.assertIn("bla.png", response_post['image'])
            self.assertIsNotNone(response_post['created_at'])
            self.assertIsNotNone(response_post['updated_at'])

    def test_pagination(self):
        # when
        result_1 = self.get_posts(reverse("api_v1:posts:post"))
        response_1 = json.loads(result_1.content)

        result_2 = self.get_posts(response_1['next'])
        response_2 = json.loads(result_2.content)

        result_3 = self.get_posts(response_2['next'])
        response_3 = json.loads(result_3.content)

        result_4 = self.get_posts(response_2['next'][:-1] + "4")

        # then
        self.assertEqual(result_1.status_code, 200)
        self.assertEqual(result_2.status_code, 200)
        self.assertEqual(result_3.status_code, 200)
        self.assertEqual(result_4.status_code, 404)

        self.assertEqual(len(response_1['results']), 10)
        self.assertEqual(len(response_2['results']), 10)
        self.assertEqual(len(response_3['results']), 10)

    def test_get_as_unauthenticated_user(self):
        # given
        post: Post = Post.objects.first()

        # when
        result = self.get_post(post.id)
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertEqual(response['title'], post.title)
        self.assertEqual(response['description'], post.description)
        self.assertIn(reverse("api_v1:posts:post-detail", kwargs={"pk": post.id}), response['url'])
        self.assertEqual(response['author'], {"id": self.user_1.id, "username": self.user_1.username})
        self.assertEqual(response['image'].split('/')[-1], post.image.name)
        self.assertEqual(response['created_at'], self.datetime_to_string(post.created_at))
        self.assertEqual(response['updated_at'], self.datetime_to_string(post.updated_at))

    def test_get_as_authenticated_user(self):
        # given
        post: Post = Post.objects.first()

        # when
        self.authenticate(self.user_1)
        result = self.get_post(post.id)
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertEqual(response['title'], post.title)
        self.assertEqual(response['description'], post.description)
        self.assertIn(reverse("api_v1:posts:post-detail", kwargs={"pk": post.id}), response['url'])
        self.assertEqual(response['author'], {"id": self.user_1.id, "username": self.user_1.username})
        self.assertEqual(response['image'].split('/')[-1], post.image.name)
        self.assertEqual(response['created_at'], self.datetime_to_string(post.created_at))
        self.assertEqual(response['updated_at'], self.datetime_to_string(post.updated_at))

    def test_create_as_authenticated_user(self):
        # when
        self.authenticate(self.user_1)
        result = self.create_post(**self.create_data)
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 201)
        self.assertEqual(response['title'], self.create_data['title'])
        self.assertEqual(response['description'], self.create_data['description'])
        self.assertIn(reverse("api_v1:posts:post-detail", kwargs={"pk": response['id']}), response['url'])
        self.assertEqual(response['author'], {"id": self.user_1.id, "username": self.user_1.username})
        self.assertIsNotNone(response['image'])
        self.assertIsNotNone(response['created_at'])
        self.assertIsNotNone(response['updated_at'])

    def test_create_as_unauthenticated_user(self):
        # when
        result = self.create_post(**self.create_data)

        # then
        self.assertEqual(result.status_code, 401)

    def test_update_as_authenticated_user(self):
        # given
        post: Post = Post.objects.first()

        # when
        self.authenticate(self.user_1)
        result = self.update_post(post.id, **self.update_data)
        updated_post = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertEqual(updated_post['title'], self.update_data['title'])
        self.assertEqual(updated_post['description'], self.update_data['description'])
        self.assertIn(updated_post['image'].split("/")[-1], post.image.name)

    def test_update_as_unauthenticated_user(self):
        # given
        post: Post = Post.objects.first()

        # when
        result = self.update_post(post.id, **self.update_data)

        # then
        self.assertEqual(result.status_code, 401)

    def test_update_not_an_author(self):
        # when
        self.authenticate(self.user_1)
        created_post = json.loads(self.create_post(**self.create_data).content)
        self.authenticate(self.user_2)
        result = self.update_post(created_post['id'], **self.update_data)

        # then
        self.assertEqual(result.status_code, 403)

    def test_delete_as_authenticated_user(self):
        # given
        post: Post = Post.objects.filter(author=self.user_1).first()

        # when
        self.authenticate(self.user_1)
        result = self.delete_post(post.id)

        # then
        self.assertEqual(result.status_code, 204)

    def test_delete_as_unauthenticated_user(self):
        # given
        post: Post = Post.objects.filter(author=self.user_1).first()

        # when
        result = self.delete_post(post.id)

        # then
        self.assertEqual(result.status_code, 401)

    def test_delete_not_an_author(self):
        # when
        self.authenticate(self.user_1)
        created_post = json.loads(self.create_post(**self.create_data).content)
        self.authenticate(self.user_2)
        result = self.delete_post(created_post['id'])

        # then
        self.assertEqual(result.status_code, 403)

    @classmethod
    def tearDownClass(cls):
        super(PostApiTest, cls).tearDownClass()
        User.objects.all().delete()
        Post.objects.all().delete()
