import json
from typing import List

from django.urls import reverse

from apps.common.testcase import InstalikeTestCase
from apps.posts.models import User, Post, Comment


class CommentApiTest(InstalikeTestCase):

    @classmethod
    def setUpTestData(cls):
        super(CommentApiTest, cls).setUpTestData()
        cls.created_comments: List[Comment] = []
        cls.post_1 = Post.objects.create(title="bla", description="bla2", author=cls.user_1)
        cls.post_2 = Post.objects.create(title="bla", description="bla2", author=cls.user_1)

        for i in range(30):
            cls.created_comments.append(
                Comment.objects.create(description="Desc", author=cls.user_1, post=cls.post_1))

        for i in range(30):
            cls.created_comments.append(
                Comment.objects.create(description="Desc", author=cls.user_1, post=cls.post_2))

    def setUp(self):
        super(CommentApiTest, self).setUp()
        self.create_data = {"description": "hehe"}
        self.update_data = {"description": "hehe22"}

    def test_get_list(self):
        # when
        result = self.get_comments(self.post_1.id)
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertIsNotNone(response['next'])
        self.assertIsNone(response['previous'])
        self.assertEqual(response['count'], 30)

        for index, response_comment in enumerate(response['results']):
            created_comment = self.created_comments[index]
            self.assertEqual(response_comment['description'], created_comment.description)
            self.assertIn(
                reverse("api_v1:posts:comments:comment-detail",
                        kwargs={"post_id": self.post_1.id, "pk": response_comment['id']}), response_comment['url'])
            self.assertEqual(response_comment['author'], {"id": self.user_1.id, "username": self.user_1.username})
            self.assertIsNotNone(response_comment['created_at'])
            self.assertIsNotNone(response_comment['updated_at'])

    def test_pagination(self):
        # when
        result_1 = self.get_comments(self.post_1.id)
        response_1 = json.loads(result_1.content)

        result_2 = self.get_comments(self.post_1.id, response_1['next'])
        response_2 = json.loads(result_2.content)

        result_3 = self.get_comments(self.post_1.id, response_2['next'])
        response_3 = json.loads(result_3.content)

        result_4 = self.get_comments(self.post_1.id, response_2['next'][:-1] + "4")

        # then
        self.assertEqual(result_1.status_code, 200)
        self.assertEqual(result_2.status_code, 200)
        self.assertEqual(result_3.status_code, 200)
        self.assertEqual(result_4.status_code, 404)

        self.assertEqual(len(response_1['results']), 10)
        self.assertEqual(len(response_2['results']), 10)
        self.assertEqual(len(response_3['results']), 10)

    def test_get_as_authenticated_user(self):
        # given
        comment: Comment = Comment.objects.first()

        # when
        self.authenticate(self.user_1)
        result = self.get_comment(comment.post_id, comment.id)
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertEqual(response['description'], comment.description)
        self.assertIn(
            reverse("api_v1:posts:comments:comment-detail", kwargs={"post_id": comment.post_id, "pk": comment.id}),
            response['url'])
        self.assertEqual(response['author'], {"id": self.user_1.id, "username": self.user_1.username})
        self.assertEqual(response['created_at'], self.datetime_to_string(comment.created_at))
        self.assertEqual(response['updated_at'], self.datetime_to_string(comment.updated_at))

    def test_get_as_unauthenticated_user(self):
        # given
        comment: Comment = Comment.objects.first()

        # when
        result = self.get_comment(comment.post_id, comment.id)
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertEqual(response['description'], comment.description)
        self.assertIn(
            reverse("api_v1:posts:comments:comment-detail", kwargs={"post_id": comment.post_id, "pk": comment.id}),
            response['url'])
        self.assertEqual(response['author'], {"id": self.user_1.id, "username": self.user_1.username})
        self.assertEqual(response['created_at'], self.datetime_to_string(comment.created_at))
        self.assertEqual(response['updated_at'], self.datetime_to_string(comment.updated_at))

    def test_create_as_authenticated_user(self):
        # when
        self.authenticate(self.user_1)
        result = self.create_comment(self.post_1.id, **self.create_data)
        response = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 201)
        self.assertEqual(response['description'], self.create_data['description'])
        self.assertIn(
            reverse("api_v1:posts:comments:comment-detail", kwargs={"post_id": self.post_1.id, "pk": response['id']}),
            response['url'])
        self.assertEqual(response['author'], {"id": self.user_1.id, "username": self.user_1.username})
        self.assertIsNotNone(response['created_at'])
        self.assertIsNotNone(response['updated_at'])

    def test_create_as_unauthenticated_user(self):
        # when
        result = self.create_comment(self.post_1.id, **self.create_data)

        # then
        self.assertEqual(result.status_code, 401)

    def test_update_as_authenticated_user(self):
        # given
        comment: Comment = Comment.objects.filter(author=self.user_1).first()

        # when
        self.authenticate(self.user_1)
        result = self.update_comment(comment.post_id, comment.id, **self.update_data)
        updated_comment = json.loads(result.content)

        # then
        self.assertEqual(result.status_code, 200)
        self.assertEqual(updated_comment['description'], self.update_data['description'])
        self.assertEqual(updated_comment['created_at'], self.datetime_to_string(comment.created_at))
        self.assertIsNotNone(updated_comment['updated_at'])

    def test_update_as_unauthenticated_user(self):
        # given
        comment: Comment = Comment.objects.filter(author=self.user_1).first()

        # when
        result = self.update_comment(comment.post_id, comment.id, **self.update_data)

        # then
        self.assertEqual(result.status_code, 401)

    def test_update_not_an_author(self):
        # when
        self.authenticate(self.user_1)
        created_comment = json.loads(self.create_comment(self.post_1.id, **self.create_data).content)
        self.authenticate(self.user_2)
        result = self.update_comment(self.post_1.id, created_comment['id'], **self.update_data)

        # then
        self.assertEqual(result.status_code, 403)

    def test_delete_as_authenticated_user(self):
        # when
        self.authenticate(self.user_1)
        created_comment = json.loads(self.create_comment(self.post_1.id, **self.create_data).content)
        result = self.delete_comment(self.post_1.id, created_comment['id'])

        # then
        self.assertEqual(result.status_code, 204)

    def test_delete_as_unauthenticated_user(self):
        # given
        comment: Comment = Comment.objects.filter(author=self.user_1).first()

        # when
        result = self.delete_comment(comment.post_id, comment.id)

        # then
        self.assertEqual(result.status_code, 401)

    def test_delete_not_an_author(self):
        # when
        self.authenticate(self.user_1)
        created_comment = json.loads(self.create_comment(self.post_1.id, **self.create_data).content)
        self.authenticate(self.user_2)
        result = self.delete_comment(self.post_1.id, created_comment['id'])

        # then
        self.assertEqual(result.status_code, 403)

    @classmethod
    def tearDownClass(cls):
        super(CommentApiTest, cls).tearDownClass()
        User.objects.all().delete()
        Post.objects.all().delete()
        Comment.objects.all().delete()
