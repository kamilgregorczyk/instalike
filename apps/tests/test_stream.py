import json
from typing import List

from apps.common.testcase import InstalikeTestCase
from apps.posts.models import Post, User


class TestApiStream(InstalikeTestCase):

    def setUp(self):
        self.user_data = {'email': 'ggg@me.com', 'username': 'gggghhhhh', 'password': self.user_password,
                          'password_confirm': self.user_password}

    def test_stream(self):
        users_to_create: int = 3
        posts_to_create: int = 2

        created_users: List[User] = []

        # Creates users
        for i in range(users_to_create):
            user_data = self.user_data.copy()
            user_data['email'] = "%s%s" % (i, user_data['email'])
            user_data['username'] = "%s%s" % (i, user_data['username'])
            response = self.register_user(user_data)
            self.assertEquals(response.status_code, 201)
            created_users.append(User(id=json.loads(response.content)['id'], username=user_data['username']))

        created_posts = {}
        for user in created_users:
            created_posts[user.id] = []

        # Creates posts for each user
        for user in created_users:
            self.authenticate(user, self.user_password)
            for i in range(posts_to_create):
                post_data = self.get_post_data()
                post_data['title'] = "%s-%s %s" % (user.id, i, post_data['title'])
                response = self.create_post(**post_data)
                self.assertEquals(response.status_code, 201)
                created_posts[user.id].append(json.loads(response.content))

        self.assertEquals(len([item for sublist in created_posts.values() for item in sublist]),
                          users_to_create * posts_to_create)

        # Test if logged user with no friends sees only his own posts
        self.authenticate(created_users[0], self.user_password)
        stream_response = self.get_stream()
        self.assertEquals(stream_response.status_code, 200)
        posts_from_stream = json.loads(stream_response.content)['results']
        self.assertEquals(len(posts_from_stream), posts_to_create)
        for created_post in created_posts[created_users[0].id]:
            self.assertEquals(created_post['author']['id'], created_users[0].id)
            self.assertEquals(created_post['author']['username'], created_users[0].username)

        # Adds user_2 to user_1
        follow_response = self.follow(created_users[1].id)
        self.assertEquals(follow_response.status_code, 201)

        # Tests if logged user sees his own and friend's posts
        stream_response = self.get_stream()
        self.assertEquals(stream_response.status_code, 200)
        posts_from_stream = json.loads(stream_response.content)['results']
        self.assertEquals(len(posts_from_stream), posts_to_create * 2)

        # Checks his own posts
        for created_post in created_posts[created_users[0].id]:
            self.assertEquals(created_post['author']['id'], created_users[0].id)
            self.assertEquals(created_post['author']['username'], created_users[0].username)

        # Checks his friend's posts
        for created_post in created_posts[created_users[1].id]:
            self.assertEquals(created_post['author']['id'], created_users[1].id)
            self.assertEquals(created_post['author']['username'], created_users[1].username)

        # Removes user_2 from user_1 friend list:
        unfollow_response = self.unfollow(created_users[1].id)
        self.assertEquals(unfollow_response.status_code, 204)

        # Test if logged user with no friends sees only his own posts
        stream_response = self.get_stream()
        self.assertEquals(stream_response.status_code, 200)
        posts_from_stream = json.loads(stream_response.content)['results']
        self.assertEquals(len(posts_from_stream), posts_to_create)
        for created_post in created_posts[created_users[0].id]:
            self.assertEquals(created_post['author']['id'], created_users[0].id)
            self.assertEquals(created_post['author']['username'], created_users[0].username)

    def get_post_data(self):
        return {"title": "Some title", "description": "Desc", "image": self.create_tmp_image()}

    @classmethod
    def tearDownClass(cls):
        super(TestApiStream, cls).tearDownClass()
        Post.objects.all().delete()
        User.objects.all().delete()
