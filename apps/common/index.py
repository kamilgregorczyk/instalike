from django.contrib.admindocs.views import simplify_regex
from django.http import JsonResponse
from django.urls import RegexURLResolver, RegexURLPattern
from rest_framework.views import APIView

from instalike import urls


def get_allowed_methods(callback):
    """
    Return a list of the valid HTTP methods for this endpoint.
    """
    if hasattr(callback, 'actions'):
        actions = set(callback.actions.keys())
        http_method_names = set(callback.cls.http_method_names)
        return [method.upper() for method in actions & http_method_names]

    return [
        method for method in
        callback.cls().allowed_methods if method not in ('OPTIONS', 'HEAD')
    ]


def is_api_view(callback):
    """
    Return `True` if the given view callback is a REST framework view/viewset.
    """
    cls = getattr(callback, 'cls', None)
    return (cls is not None) and issubclass(cls, APIView)


def should_include_endpoint(path, callback):
    """
    Return `True` if the given endpoint should be included.
    """
    if not is_api_view(callback):
        return False  # Ignore anything except REST framework views.

    if path.endswith('.{format}') or path.endswith('.{format}/'):
        return False  # Ignore .json style URLs.

    return True


def get_path_from_regex(path_regex):
    """
    Given a URL conf regex, return a URI template string.
    """
    path = simplify_regex(path_regex)
    path = path.replace('<', '{').replace('>', '}')
    return path


def get_urls(base_url, patterns=None, prefix='', api_version='v1', namespace=''):
    if patterns is None:
        patterns = []
        for pattern in urls.urlpatterns:
            if api_version in pattern.regex.pattern:
                patterns.append(pattern)

    api_endpoints = {namespace: []}
    for pattern in patterns:
        path_regex = prefix + pattern.regex.pattern
        if isinstance(pattern, RegexURLPattern):
            path = get_path_from_regex(path_regex)
            callback = pattern.callback
            if should_include_endpoint(path, callback):
                api_endpoints[namespace].append({"url": base_url + path, "methods": get_allowed_methods(callback)})
        elif isinstance(pattern, RegexURLResolver):

            nested_endpoints = get_urls(
                patterns=pattern.url_patterns,
                base_url=base_url,
                prefix=path_regex,
                namespace=pattern.namespace if pattern.namespace is not None else path_regex
            )
            api_endpoints[namespace].append(nested_endpoints)

    return api_endpoints


def index(request):
    base_url = 'http://' + request.META['HTTP_HOST']
    return JsonResponse({
        "Documentation": base_url + '/docs/',
        "API v1.0 Endpoints": get_urls(base_url=base_url)[''][0]['api_v1']
    })
