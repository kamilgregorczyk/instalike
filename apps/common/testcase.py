import json
from datetime import datetime
from tempfile import NamedTemporaryFile
from typing import Dict

from PIL import Image
from django.contrib.auth.models import User
from rest_framework.reverse import reverse
from rest_framework.test import APITestCase, APIClient


class InstalikeTestCase(APITestCase):

    def __init__(self, methodName='runTest'):
        self.client = APIClient
        super(InstalikeTestCase, self).__init__(methodName)

    @classmethod
    def setUpTestData(cls):
        super(InstalikeTestCase, cls).setUpTestData()
        cls.user_password = "CG@?RA+5(4$nX:ez"
        cls.user_1: User = User.objects.create_user(username="testuser", email="testuser@me.com",
                                                    password=cls.user_password)
        cls.user_2: User = User.objects.create_user(username="testuser2", email="testuser2@me.com",
                                                    password=cls.user_password)

    def setUp(self):
        self.unauthenticate()

    def follow(self, user_id):
        return self.client.post(reverse("api_v1:followers:followers-add", kwargs={"follower_id": user_id}))

    def unfollow(self, user_id):
        return self.client.delete(reverse("api_v1:followers:followers-remove", kwargs={"follower_id": user_id}))

    def datetime_to_string(self, date: datetime):
        return date.isoformat()[:-6] + 'Z'

    def register_user(self, data: Dict[str, str]):
        return self.client.post(path=reverse("api_v1:authorization:create-user"), data=data)

    def authenticate(self, user: User = None, password=None, token=None):
        self.client.credentials(
            HTTP_AUTHORIZATION="JWT " + (self.get_login_token(user, password) if token is None else token))

    def unauthenticate(self):
        self.client.credentials()

    def get_login_token(self, user: User, password=None):
        return json.loads(self.login(user, password).content)['token']

    def get_stream(self):
        return self.client.get(reverse("api_v1:posts:stream"))

    def login(self, user: User, password=None):
        return self.client.post(reverse("api_v1:authorization:get-token"),
                                data={"username": user.username,
                                      "password": self.user_password if password is None else password})

    def verify_token(self, token: str):
        return self.client.post(reverse("api_v1:authorization:verify-token"), data={"token": token})

    def refresh_token(self, token: str):
        return self.client.post(reverse("api_v1:authorization:refresh-token"), data={"token": token})

    def create_user(self, username, password=None):
        return User.objects.create_user(username=username, email=username + "@me.com",
                                        password=self.user_password if password is None else password)

    def get_post(self, post_id: int):
        return self.client.get(reverse("api_v1:posts:post-detail", kwargs={"pk": post_id}))

    def get_posts(self, path=None):
        if path is None:
            path = reverse("api_v1:posts:post")
        return self.client.get(path)

    def create_post(self, **kwargs):
        return self.client.post(reverse("api_v1:posts:post"), data=kwargs)

    def update_post(self, post_id: int, **kwargs):
        return self.client.put(reverse("api_v1:posts:post-detail", kwargs={"pk": post_id}),
                               data=kwargs)

    def delete_post(self, post_id: int):
        return self.client.delete(reverse("api_v1:posts:post-detail", kwargs={"pk": post_id}))

    def get_comment(self, post_id: int, comment_id: int):
        return self.client.get(
            reverse("api_v1:posts:comments:comment-detail", kwargs={"post_id": post_id, "pk": comment_id}))

    def get_comments(self, post_id: int = None, path=None):
        if post_id is not None and path is None:
            path = reverse("api_v1:posts:comments:comment", kwargs={"post_id": post_id})
        return self.client.get(path)

    def create_comment(self, post_id: int, **kwargs):
        return self.client.post(reverse("api_v1:posts:comments:comment", kwargs={"post_id": post_id}), data=kwargs)

    def update_comment(self, post_id: int, comment_id: int, **kwargs):
        return self.client.put(
            reverse("api_v1:posts:comments:comment-detail", kwargs={"post_id": post_id, "pk": comment_id}),
            data=kwargs)

    def delete_comment(self, post_id: int, comment_id: int):
        return self.client.delete(
            reverse("api_v1:posts:comments:comment-detail", kwargs={"post_id": post_id, "pk": comment_id}))

    @classmethod
    def create_tmp_image(cls):
        img = Image.new('RGB', (250, 250), "black")  # create a new black image
        pixels = img.load()  # create the pixel map

        for i in range(img.size[0]):  # for every col:
            for j in range(img.size[1]):  # For every row
                pixels[i, j] = (i, j, 100)  # set the colour accordingly
        tmp_file = NamedTemporaryFile(suffix='.jpg')
        img.save(tmp_file)
        tmp_file.seek(0)
        return tmp_file
