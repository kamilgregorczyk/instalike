#!/usr/bin/env bash
version=$1
if [ ! -z "$version" ]
then
    docker build -t registry.gitlab.com/kamilgregorczyk/instalike:${version} .
    docker push registry.gitlab.com/kamilgregorczyk/instalike:${version}
else
    docker build -t registry.gitlab.com/kamilgregorczyk/instalike .
    docker push registry.gitlab.com/kamilgregorczyk/instalike

fi