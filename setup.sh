#!/usr/bin/env bash
pip install -r ./requirements.txt

echo "Wait until database is ready..."
while ! nc -z ${DB_HOST:-localhost} 5432
do
	echo "Retrying..."
	sleep 2
done

python manage.py migrate
python manage.py collectstatic --no-input