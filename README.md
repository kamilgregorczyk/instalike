# Instalike
It's a very simple rest backend which has instagram like functionality.

## Things it can do

1. CRUD operations on posts.
1. CRUD operations on comments.
1. Start/Stop following someone.
1. List followers for user and by whom he's followed.
1. List all posts and a stream, a list of posts based on followers. 
1. Register user.
1. Refresh/Validate/Get JWT token.

All endpoints are avaliable on / page, there's also a auto generated documentation on /docs/ url.

## Things it cannot do yet:

1. Likes entity.
1. Privacy.
1. Private profiles.
1. Lack of kubernetes deployment config.


## Authentication

Basic's django authentication was replaced by JWT provided by the djangorestframework-jwt module.

## Requirements

* Python 3.6
* Django 1.11
* Django Rest Framework
* Django Rest Framework JWT
* PostgreSQL > 9.6
* Psycopg 
